import { expect } from 'chai';

function somme(nombre1, nombre2 = 0) {
    if (typeof nombre1 !== 'number' || typeof nombre2 !== 'number') {
        throw new Error('paramètre invalide');
    }
    return nombre1 + nombre2;
}

describe('Tests pour la fonction somme()', function () {
    it('Avec les paramètres 5 et 3, doit retourner 8', function () {
        expect(somme(5, 3)).to.equal(8);
    });

    it('Avec le paramètre 5 et un paramètre manquant, doit retourner 5', function () {
        expect(somme(5)).to.equal(5);
    });

    it("Avec un paramètre non-numérique, doit lever l'exception 'paramètre invalide'", function () {
        expect(() => somme(5, 'cinq')).to.throw(Error, 'paramètre invalide');
    });
});
